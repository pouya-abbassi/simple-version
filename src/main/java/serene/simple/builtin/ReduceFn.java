/**
 * Serene (simple) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.simple.builtin;

import java.util.List;
import java.util.function.Function;

import serene.simple.IDoc;
import serene.simple.IScope;
import serene.simple.ListNode;
import serene.simple.SereneException;


public class ReduceFn extends AFn implements IDoc {
  public String fnName() {
    return "reduce";
  };

  public String getDoc() {
    return "(reduce f coll initial-value): Reduce the COLL by applying F.";
  }

  public Object eval(IScope scope) throws SereneException{
    List<Object> args = this.arguments();

    if (args.size() != 3) {
      throw new SereneException(
        String.format("reduce expects 3 parameters. Got %s", args.size()));
    }

    Function<Object,Object> fn = (Function<Object, Object>) args.get(0);
    ListNode<Object> coll = (ListNode<Object>) args.get(1);
    Object initialValue = args.get(2);

    while(true) {
      Object x = coll.first();

      if (x == null)
        break;

      Object[] fnArgs = { initialValue, x };
      coll = coll.rest();
      initialValue = fn.apply(fnArgs);
    }

    return initialValue;
  }
}
