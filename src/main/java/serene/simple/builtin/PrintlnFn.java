/**
 * Serene (simple) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.simple.builtin;

import java.util.List;

import serene.simple.IScope;


public class PrintlnFn extends AFn {
  public String fnName() {
    return "println";
  };

  public Object eval(IScope scope) {
    List<Object> args = this.arguments();

    if (args.size() == 0) {
      System.out.println("");
      return null;
    }

    String output = args.get(0).toString();

    for(Object x: args.subList(1, args.size())) {
      output = output + " " + x.toString();
    }

    System.out.println(output);
    return null;
  }
}
