/**
 * Serene (simple) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.simple;

public class SymbolNode extends Node {
  public String name;

  public SymbolNode(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Node other) {
    if (other instanceof SymbolNode) {
      SymbolNode n = (SymbolNode) other;

      if (this.name.equals(n.name)) {
        return true;
      }

      return false;
    }
    return super.equals(other);
  }

  @Override
  public Object eval(IScope scope) {
    return scope.lookupSymbol(this.name);
  }

  @Override
  public String toString() {
    return this.name;
  }
}
