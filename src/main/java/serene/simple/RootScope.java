/**
 * Serene (simple) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.simple;

import java.util.HashMap;
import java.util.Map;

import serene.simple.builtin.*;


public class RootScope extends AScope {

  private final Map<String, Object> symbolsMapping = new HashMap<String, Object>() {{
      put("println", new PrintlnFn());
      put("quit", new QuitFn());
      put("+", new PlusFn());
      put("-", new MinusFn());
      put("*", new TimesFn());
      put("/", new ObelusFn());
      put("mod", new ModFn());
      put("=", new EqFn());
      put(">", new GtFn());
      put(">=", new GteFn());
      put("<", new LtFn());
      put("<=", new LteFn());
      put("and", new AndFn());
      put("or", new OrFn());
      put("not", new NotFn());
      put("conj", new ConjFn());
      put("cons", new ConjFn());
      put("count", new CountFn());
      put("reverse", new ReverseFn());
      put("list", new ListFn());
      put("first", new FirstFn());
      put("rest", new RestFn());
      put("doc", new DocFn());
      put("reduce", new ReduceFn());
      put("System", System.class);
      put("Boolean", Boolean.class);
      put("String", String.class);
      put("new", new NewFn());
      put("now", new NowFn());
    }};




  public RootScope() {
    this(null);
  }

  public RootScope(IScope parent) {
    this.parentScope = null;
  }

  @Override
  public Map<String, Object> symbols() {
    return this.symbolsMapping;
  }
}
