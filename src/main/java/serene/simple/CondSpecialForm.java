/**
 * Serene (simple) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.simple;

public class CondSpecialForm extends SpecialForm {

  private ListNode<Node> conditions;

  public CondSpecialForm(ListNode<Node> listNode) {
    super(listNode);

    if (!(listNode.rest().first() instanceof ListNode)) {
      throw new SereneException("Let bindings should be a list of pairs.");
    }

    this.conditions = (ListNode<Node>) listNode.rest();
  }

  @Override
  public Object eval(IScope scope) {
    for (Object x : this.conditions) {
      if (!(x instanceof ListNode))
        throw new SereneException(
          String.format("Expect a pair instead of '%s' for 'cond'.",
                        x.toString()));

      Object pred = ((ListNode<Node>) x).first().eval(scope);

      if (pred != null && pred != Boolean.FALSE) {
        return ((ListNode<Node>) x).rest().first().eval(scope);
      }
    }

    return null;
  }
}
