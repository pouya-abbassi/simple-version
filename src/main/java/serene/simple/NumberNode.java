/**
 * Serene (simple) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.simple;

/**
 * A poor implementation of a Node number.
 *
 * Why poor?
 * first of all it allocates two types of number to hold one.
 */
public class NumberNode extends Node {
  private boolean isDouble = false;
  private Double doubleValue;
  private Long longValue;

  public NumberNode(String v) {
    if (v.contains(".")) {
      this.doubleValue = Double.parseDouble(v);
      this.isDouble = true;
    }
    else {
      this.longValue = Long.parseLong(v, 10);
    }
  }

  @Override
  public Object eval(IScope scope) {
    return this.value();
  }

  @Override
  public String toString() {
    return String.format("%s", this.value());
  }

  private Object value() {
    if (isDouble) {
      return doubleValue;
    }
    return this.longValue;
  }
}
