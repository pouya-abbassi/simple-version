all:
	./gradlew clean
	./gradlew distZip

repl:
	./gradlew compileJava && rlwrap java -cp build/classes/java/main/ serene.simple.Main

run:
	./gradlew compileJava && java -cp build/classes/java/main/ serene.simple.Main ${PWD}/test.srns

docs:
	npx docco src/**/*.java
