# Serene (Simple) version
**Serene simple** is a lisp that is implemented in Java as a proof of concept and as an experience
to collect data on some of the ideas and implementations that I have. For the full story checkout
my [blog](https://lxsameer.com/).

## Requirements
* JDK >= 1.8
* rlwrap
* gradle

## Repl
in order to run the REPL, run `make repl`
